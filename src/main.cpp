#include <stdio.h>

#include "zlib.h"

#include "ft2build.h"
#include FT_FREETYPE_H

static void
test_zlib()
{
	z_stream stream;
	stream.zalloc = Z_NULL;
	stream.zfree = Z_NULL;
	stream.opaque = Z_NULL;

	auto error = deflateInit(&stream, Z_DEFAULT_COMPRESSION);
	if (error != Z_OK)
	{
		fprintf(stderr, "Failed to initialise zlib.\n");
		return;
	}

	deflateEnd(&stream);
}

static void
test_freetype()
{
	FT_Library library;

	auto error = FT_Init_FreeType(&library);
	if (error)
	{
		fprintf(stderr, "Failed to initialise FreeType.\n");
		return;
	}

	FT_Done_FreeType(library);
}

int
main(int argc, char* argv[])
{
	fprintf(stdout, "Testing zlib\n");
	test_zlib();
	fprintf(stdout, "Test successful\n");

	fprintf(stdout, "Testing FreeType\n");
	test_freetype();
	fprintf(stdout, "Test successful\n");

	return 0;
}
